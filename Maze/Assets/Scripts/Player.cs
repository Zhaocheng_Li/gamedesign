﻿using System.Collections;
using System.Collections.Generic;
using Completed;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private BoxCollider2D boxCollider;

    public Text text;

    private Rigidbody2D rb2D;

    private Animation animator;
    protected  void Start()
    {
//        animator = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
        rb2D = GetComponent<Rigidbody2D>();
        text.enabled = false;


    }

    private void Update()
    {
//        if (!GameManager.instance.)
//        {
//            
//        }
        int horizontal = 0;
        int vertical = 0;
        

//        horizontal = (int) (Input.GetAxisRaw("Horizontal"));
//        vertical = (int) (Input.GetAxisRaw("Vertical"));

//        if (horizontal != 0)
//        {
//            vertical = 0;
//        }
//
//        Input.GetKeyDown()
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            vertical = 1;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {

            vertical = -1;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            horizontal = -1;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            horizontal = 1;
        }
        Vector2 start = transform.position;

        Vector2 end = start + new Vector2(horizontal, vertical);

        boxCollider.enabled = false;

        RaycastHit2D hit = Physics2D.Linecast(start, end);

        boxCollider.enabled = true;

        if (hit.transform == null)
        {
            
//            Vector3 newPosition = Vector3.MoveTowards(rb2D.position, end);
            rb2D.MovePosition(new Vector3(end.x, end.y, 0f));
        }

        //        if (horizontal != 0 || vertical != 0)
        //        {
        ////            AttemptMove<Wall>();
        //            RaycastHit2D hit;
        //            Move(horizontal, vertical, out hit);
        //        }
    }


    //    protected override void OnCantMove<T>(T hitCompoent)
    //    {
    //        throw new System.NotImplementedException();
    //    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit")
        {
            text.enabled = true;
        }
    }



}
