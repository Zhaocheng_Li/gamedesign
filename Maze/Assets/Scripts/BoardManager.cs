﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{

    private Transform boardHolder;

    public int colums = 13;
    public int rows = 10;

    public GameObject outerWallTile;


    public GameObject exit;

    int[,] mazeArray =
    { {1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,1,0,1,0,1,0,0,0,0,0,1},
        {1,0,1,0,0,0,1,0,1,1,1,0,1},
        {1,0,0,0,1,1,1,0,0,0,0,0,1},
        {1,0,1,0,0,0,0,0,1,1,1,0,1},
        {1,0,1,0,1,1,1,0,1,0,0,0,1},
        {1,0,1,0,1,0,0,0,1,1,1,0,1},
        {1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,1,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1}

    };

    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }


    public void SetupScene(int level)
    {
        BoardSetup();
    }

    void BoardSetup()
    {
      
        boardHolder = new GameObject("Board").transform;
  
        for (int x = 0; x < colums; x++)
        {
            for (int y = 0; y < rows ; y++)
            {
//                if (x == -1 || x == colums || y == -1 || y == rows)
//                {
               

                if (mazeArray[x, y] == 1)
                {
                    GameObject toInstantiate = outerWallTile;
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(boardHolder);
                }

                  

//                }


            }
           

        }
        Instantiate(exit, new Vector3(colums - 2, rows - 2, 0f), Quaternion.identity);
    }
}
